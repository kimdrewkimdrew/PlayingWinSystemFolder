#-*-coding: UTF-8 -*-
# Made by kdr
import ctypes
from os import system
if (ctypes.windll.shell32.IsUserAnAdmin() == True):
    # 관리자 권한으로 실행 됨
    # 레지스트리 파일 생성, 등록 및 삭제 
    system('move nul > takeownership.reg')

    system('echo Windows Registry Editor Version 5.00 > takeownership.reg')
    system('echo [HKEY_CLASSES_ROOT\\*\\shell\\takeownership] >> takeownership.reg')
    system('echo @="Take ownership" >> takeownership.reg')
    system('echo "HasLUAShield"="" >> takeownership.reg')
    system('echo "NoWorkingDirectory"="" >> takeownership.reg')
    
    system('echo [HKEY_CLASSES_ROOT\\*\\shell\\takeownership\\command] >> takeownership.reg')
    system('echo @="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F" >> takeownership.reg')
    system('echo "IsolatedCommand"="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F" >> takeownership.reg')

    system('echo [HKEY_CLASSES_ROOT\\exefile\\shell\\takeownership] >> takeownership.reg')
    system('echo @="Take ownership" >> takeownership.reg')
    system('echo "HasLUAShield"="" >> takeownership.reg')
    system('echo "NoWorkingDirectory"="" >> takeownership.reg')

    system('echo [HKEY_CLASSES_ROOT\\exefile\\shell\\takeownership\\command] >> takeownership.reg')
    system('echo @="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F" >> takeownership.reg')
    system('echo "IsolatedCommand"="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F" >> takeownership.reg')

    system('echo [HKEY_CLASSES_ROOT\\dllfile\\shell\\takeownership] >> takeownership.reg')
    system('echo @="Take ownership" >> takeownership.reg')
    system('echo "HasLUAShield"="" >> takeownership.reg')
    system('echo "NoWorkingDirectory"="" >> takeownership.reg')

    system('echo [HKEY_CLASSES_ROOT\\dllfile\\shell\\takeownership\\command] >> takeownership.reg')
    system('echo @="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F" >> takeownership.reg')
    system('echo "IsolatedCommand"="cmd.exe /c takeown /f \"%1\" && icacls \"%1\" /grant administrators:F" >> takeownership.reg')

    system('echo [HKEY_CLASSES_ROOT\\Directory\\shell\\takeownership] >> takeownership.reg')
    system('echo @="Take ownership" >> takeownership.reg')
    system('echo "HasLUAShield"="" >> takeownership.reg')
    system('echo "NoWorkingDirectory"="" >> takeownership.reg')

    system('echo [HKEY_CLASSES_ROOT\\Directory\\shell\\takeownership\\command] >> takeownership.reg')
    system('echo @="cmd.exe /c takeown /f \"%1\" /r /d y && icacls \"%1\" /grant administrators:F /t" >> takeownership.reg')
    system('echo "IsolatedCommand"="cmd.exe /c takeown /f \"%1\" /r /d y && icacls \"%1\" /grant administrators:F /t" >> takeownership.reg')

    system('reg import takeownership.reg')
    system('del takeownership.reg')    
   
    # 파일 권한 TrustedInstaller => Administrator (시스템 파일 전부 수정 가능)
    if (path.isdir('C:\\Windows\\SysWOW64') == True): # x64 (64bit)
        system('takeown /F "C:\\Windows\\System32" /R /D Y & icacls "C:\\Windows\\System32" /grant administrators:F /T')
        system('cls')
        system('takeown /F "C:\\Windows\\SysWOW64" /R /D Y & icacls "C:\\Windows\\SysWOW64" /grant administrators:F /T')
        system('cls')
    else: # x86 (32bit)
        system('takeown /F "C:\\Windows\\System32" /R /D Y & icacls "C:\\Windows\\System32" /grant administrators:F /T')
    """
    이제 시스템폴더를 관리자 권한만으로 돌아다닐 수 있음 (System32, SysWOW64)
    """

    # 레지스트리 파일 생성, 등록 및 삭제
    """
    # 파일 권한 Administrator => TrustedInstaller (시스템 파일 수정 불가능)
    os.system('takeown /F "C:\\Windows\\System32" /R /D Y & icacls "C:\\Windows\\System32" /grant TrustedInstaller:F /C /T')
    os.system('takeown /F "C:\\Windows\\SysWOW64" /R /D Y & icacls "C:\\Windows\\SysWOW64" /grant TrustedInstaller:F /C /T')
    """
    system('move nul > retakeownership.reg')
    system('echo Windows Registry Editor Version 5.00 > retakeownership.reg')
    system('echo [-HKEY_CLASSES_ROOT\\*\\shell\\takeownership] >> retakeownership.reg')
    system('echo [-HKEY_CLASSES_ROOT\\exefile\\shell\\takeownership] >> retakeownership.reg')
    system('echo [-HKEY_CLASSES_ROOT\\dllfile\\shell\\takeownership] >> retakeownership.reg')
    system('echo [-HKEY_CLASSES_ROOT\\Directory\\shell\\takeownership] >> retakeownership.reg')

    system('reg import retakeownership.reg')
    system('del retakeownership.reg')
    system('cls')
    system('exit')
else:
    # 관리자 권한 아닐때는 실행 안됨
    print """
This program can be run as ADMINISTRATOR!
This program can be run as ADMINISTRATOR!
This program can be run as ADMINISTRATOR!
This program can be run as ADMINISTRATOR!
This program can be run as ADMINISTRATOR!
This program can be run as ADMINISTRATOR!
This program can be run as ADMINISTRATOR!
This program can be run as ADMINISTRATOR!
This program can be run as ADMINISTRATOR!
This program can be run as ADMINISTRATOR!
    """
    system('pause')
    exit(1)
